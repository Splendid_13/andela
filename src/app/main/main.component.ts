import { Component, OnInit } from "@angular/core";
import "rxjs/add/operator/map";
import { Headers, Http, Response, RequestOptions } from "@angular/http";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.css"]
})
export class MainComponent implements OnInit {
  //initialize variables
  title = "darey";
  authors: any[];
  coverimage;
  description;
  finaldesc;
  gotten;
  found = false;
  ISBN;
  error = false;
  networkstatus = false;

  results: any[];

  constructor(private http: Http) {
    this.http = http;
  }

  ngOnInit() {
    this.get();
  }

  // pass isbn from template
  number(code) {
    // console.log(code.value);
    Number;
    this.ISBN = code.value;

    this.finder(code.value);
    // this.go();
  }

  //check if it is already stored in the localstorage
  finder(isbn) {
    this.gotten = localStorage.getItem("saved");
    let got = JSON.parse(this.gotten);
    if (got === null) {
      this.go();
    } else {
      const mag = got.findIndex(got => got.ISBN === isbn);
      // console.log(mag);
      if (mag === -1) {
        this.found = true;
        this.go();
        this.error = false;
      } else {
        // console.log(got);
        this.authors = got[mag].Authors;
        this.title = got[mag].Title;
        this.description = this.shortdesc(got[mag].Description);
        this.coverimage = got[mag].CoverImage;
        this.error = false;
      }
    }
  }

  //get stored data from localstorage
  get() {
    let getlocal = localStorage.getItem("saved");
    this.gotten = JSON.parse(getlocal);
    // console.log(this.gotten);
  }

  //get first 10 words from description
  shortdesc(word) {
    let str2 = word.replace(/(([^\s]+\s\s*){10})(.*)/, "$1");
    return str2;
  }

  //get data from Booksnomad API
  go() {
    if (navigator.onLine === true) {
      this.networkstatus = false;
      this.http
        .get("https://www.booknomads.com/api/v0/isbn/" + this.ISBN)
        .map((response: Response) => response.json())
        .subscribe(
          res => {
            // console.log(res);
            this.authors = res.Authors;
            this.title = res.Title;
            this.description = this.shortdesc(res.Description);
            this.coverimage = res.CoverThumb;

            this.error = false;
            ///
            let getlocal = localStorage.getItem("saved");
            let got = JSON.parse(getlocal);

            //if localstorage is empty, create new item
            if (got === null) {
              let gotISBN = [
                {
                  ISBN: res.ISBN,
                  CoverImage: res.CoverThumb,
                  Title: res.Title,
                  Authors: res.Authors,
                  Description: res.Description
                }
              ];
              localStorage.setItem("saved", JSON.stringify(gotISBN));
              this.get();
            }

            //if isbn is not in localstorage, append it to item in localstorage
            if (this.found === true) {
              let gotISBN2 = {
                ISBN: res.ISBN,
                CoverImage: res.CoverThumb,
                Title: res.Title,
                Authors: res.Authors,
                Description: res.Description
              };

              var a = [];

              a = JSON.parse(localStorage.getItem("saved"));

              a.push(gotISBN2);

              // alert(a);
              localStorage.setItem("saved", JSON.stringify(a));
            }
          },
          err => {
            // let error = JSON.parse(err.text());
            // console.log(err);
            // console.log(err._body);
            this.error = true;

            return false;
          }
        );
    } else {
      this.networkstatus = true;
    }
  }
}
