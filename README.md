# AndelaTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.
Angular Version : Angular 5

## Installation

Go to https://bitbucket.org/Splendid_13/andela/overview.

You can either clone the repository or download it 

## Downloading
1. On Overview Page, Click on Downloads.
2. After downloading, Unzip the folder.
3. Open the project in an editor, and through command line, run "npm install" to download node modules.
4. Run ng serve for a dev server. Navigate to http://localhost:4200/.
5. If another application is using port :4200, use another port by running "ng serve --port{{port_number}}".

## Cloning project
1. Create a new folder on your machine.
2. initialize git by running "git init"
3. Run "git clone https://bitbucket.org/Splendid_13/andela.git""
4. Navigate to the andela folder by running "cd andela"
5. Run "npm install" to download node modules.
6. Run ng serve for a dev server. Navigate to http://localhost:4200/.
7. If another application is using port :4200, use another port by running "ng serve --port{{port_number}}".

